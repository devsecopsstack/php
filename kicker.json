{
  "name": "PHP",
  "description": "Build, test and analyse your [PHP](https://www.php.net/) projects",
  "template_path": "templates/gitlab-ci-php.yml",
  "kind": "build",
  "prefix": "php",
  "is_component": true,
  "variables": [
    {
      "name": "PHP_IMAGE",
      "description": "The Docker image used to run PHP - **set the version required by your project**",
      "default": "registry.hub.docker.com/library/php:latest"
    },
    {
      "name": "PHP_PROJECT_DIR",
      "description": "The PHP project root directory",
      "default": ".",
      "advanced": true
    }
  ],
  "features": [
    {
      "id": "phpunit",
      "name": "PHPUnit",
      "description": "[PHPUnit](https://docs.phpunit.de/) tests\n\nAutomatically enabled if a PHPUnit [XML configuration file](https://docs.phpunit.de/en/11.0/configuration.html#appendixes-configuration) is found in the project (`phpunit.xml`)",
      "disable_with": "PHP_UNIT_DISABLED",
      "variables": [
        {
          "name": "PHP_UNIT_ARGS",
          "description": "Additional PHPUnit [options](https://docs.phpunit.de/en/11.0/textui.html#command-line-options)",
          "advanced": true
        }
      ]
    },
    {
      "id": "codesniffer",
      "name": "PHP_CodeSniffer",
      "description": "[PHP_CodeSniffer](https://github.com/squizlabs/PHP_CodeSniffer) analysis",
      "disable_with": "PHP_CODESNIFFER_DISABLED",
      "variables": [
        {
          "name": "PHP_CODESNIFFER_ARGS",
          "description": "PHP_CodeSniffer [options](https://github.com/squizlabs/PHP_CodeSniffer/wiki/Configuration-Options)\n\nEither use this variable or use an [XML configuration file](https://github.com/squizlabs/PHP_CodeSniffer/wiki/Advanced-Usage#using-a-default-configuration-file) in your project.",
          "advanced": true
        }
      ]
    },
    {
      "id": "sbom",
      "name": "Software Bill of Materials",
      "description": "This job generates a file listing all dependencies using [@cyclonedx/cyclonedx-php](https://github.com/CycloneDX/cyclonedx-php-composer)",
      "disable_with": "PHP_SBOM_DISABLED",
      "variables": [
        {
          "name": "PHP_SBOM_VERSION",
          "description": "Version of the cyclonedx-php-composer used for SBOM analysis",
          "advanced": true
        },
        {
          "name": "PHP_SBOM_OPTS",
          "description": "[`cyclonedx/cyclonedx-php` options](https://github.com/CycloneDX/cyclonedx-php-composer#usage) used for SBOM analysis",
          "advanced": true
        }
      ]
    },
    {
      "id": "outdated",
      "name": "composer outdated",
      "description": "Shows the list of installed packages that have updates available (uses [`composer outdated`](https://getcomposer.org/doc/03-cli.md#outdated))",
      "variables": [
        {
          "name": "PHP_OUTDATED_OPTS",
          "description": "[`composer outdated` options](https://getcomposer.org/doc/03-cli.md#outdated)",
          "default": "--direct",
          "advanced": true
        }
      ]
    },
    {
      "id": "audit",
      "name": "composer audit",
      "description": " Scan your dependencies for vulnerabilities with [`composer audit`](https://getcomposer.org/doc/03-cli.md#audit)",
      "disable_with": "PHP_COMPOSER_AUDIT_DISABLED",
      "variables": [
        {
          "name": "PHP_COMPOSER_AUDIT_OPTS",
          "description": "[`composer audit` options](https://getcomposer.org/doc/03-cli.md#audit)",
          "default": "--locked",
          "advanced": true
        }
      ]
    }
  ]
}
